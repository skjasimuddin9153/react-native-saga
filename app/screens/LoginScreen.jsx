// RegisterScreen.tsx
import React, { useCallback, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, TextInput, Snackbar } from 'react-native-paper';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { registrationSuccess } from '../features/slice/GlobalSlice';
import { loginUserRequest, registerUserRequest, registrationError } from '../features/saga/auth/authAction';
import { useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native';
import validations from '../validations';

const LoginScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation()
  const [snackBarVisible, setSnackBarVisible] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');

  const { error , user ,loader} = useSelector((state) => state.globalReducer)
  const onDismissSnackBar = () => setSnackBarVisible(false);


  const handleRegister = (values) => {
    console.log('values', values)
    dispatch(loginUserRequest({ email: values.email, password: values.password }));

   

  };

  console.log('user', user)
  useEffect(() => {
    if (error) {
      setErrorMessage(error);
      setSnackBarVisible(true);
      // Clear error message after displaying it
      dispatch(registrationError(null));
    }
    if(user!=null){
      navigation.replace('Product')
    }
  }, [error,user]);


  return (
    <View style={styles.container}>
      <Formik
        initialValues={{ email: '', password: '' }}
        validationSchema={validations.authValidation}
        onSubmit={handleRegister}>
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
          <>
            <TextInput
              style={{ marginBottom: 10 }}
              label="Email"
              value={values.email}
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              error={Boolean(touched.email) && Boolean(errors.email)}
            />
            <TextInput
              label="Password"
              secureTextEntry
              value={values.password}
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              error={Boolean(touched.password) && Boolean(errors.password)}

            />
            <Button loading={loader} mode="contained" onPress={() => handleSubmit()} style={styles.button}>
              Login
            </Button>
          </>
        )}
      </Formik>

      <Snackbar
        visible={snackBarVisible}
        onDismiss={() => setSnackBarVisible(false)}
        action={{
          label: 'Close',
          onPress: () => {
            setSnackBarVisible(false);
          },
        }}>
        {errorMessage}
      </Snackbar>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  button: {
    marginTop: 20,
  },
});

export default LoginScreen;
