import { SafeAreaProvider } from "react-native-safe-area-context";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import RegisterScreen from "../screens/RegisterScreen";
import LoginScreen from "../screens/LoginScreen";
import ProductScreen from "../screens/ProductScreen";
import AuthNavigation from "./AuthNavigation";

const screen = [
  
  {
    name:'Product',
    component:ProductScreen
  },
  {
    name:'Login',
    component:LoginScreen
  }
];

const HomeNavigation = ({ route }) => {

  const Stack = createNativeStackNavigator();
  return (
    <SafeAreaProvider>
    
       
        <Stack.Navigator
          initialRouteName={'Product'}
          screenOptions={{
            headerShown: false,
            headerSearchBarOptions: {
              cancelButtonText: "Cancel",
            },
          }}
        >
          {screen.map((sc,index) => {
            return (
              <>
                <Stack.Screen
                  name={sc.name}
                  component={sc.component}
                  key={sc.name}
                />
              </>
            );
          })}
        </Stack.Navigator>
    
    </SafeAreaProvider>
  );
};

export default HomeNavigation;
