import { SafeAreaProvider } from "react-native-safe-area-context";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import RegisterScreen from "../screens/RegisterScreen";
import LoginScreen from "../screens/LoginScreen";
import ProductScreen from "../screens/ProductScreen";

const screen = [
  {
    name: 'Home',
    component: RegisterScreen,
  },
  {
    name: 'Login',
    component: LoginScreen,
  },
  {
    name:'Product',
    component:ProductScreen
  }
];

const AuthNavigation = ({ route }) => {

  const Stack = createNativeStackNavigator();
  return (
    <SafeAreaProvider>
    
       
        <Stack.Navigator
          initialRouteName={'Home'}
          screenOptions={{
            headerShown: false,
            headerSearchBarOptions: {
              cancelButtonText: "Cancel",
            },
          }}
        >
          {screen.map((sc,index) => {
            return (
              <>
                <Stack.Screen
                  name={sc.name}
                  component={sc.component}
                  key={sc.name}
                />
              </>
            );
          })}
        </Stack.Navigator>
    
    </SafeAreaProvider>
  );
};

export default AuthNavigation;
